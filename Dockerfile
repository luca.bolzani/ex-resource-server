FROM amd64/openjdk:17

VOLUME /tmp

ADD run.sh /run.sh
RUN chmod +x /run.sh
ADD target/*.jar app.jar

ENTRYPOINT ./run.sh
