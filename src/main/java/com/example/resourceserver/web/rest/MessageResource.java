package com.example.resourceserver.web.rest;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class MessageResource {

    @GetMapping("/")
    public String retrieveRootMessage(Authentication authentication) {
        System.out.println("Authentication: " + authentication.getName());
        return "Root message";
    }

    @GetMapping("/message")
    public String retrieveMessage(Authentication authentication) {
        System.out.println("Authentication: " + authentication.getName());
        return "User authenticated!";
    }

}
