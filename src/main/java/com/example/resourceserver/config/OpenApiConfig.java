package com.example.resourceserver.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Optional;

@Configuration
public class OpenApiConfig {

    @Value("${swagger-custom.token-url}")
    private String tokenUrl;

    @Value("${swagger-custom.server-url}")
    private String serverUrl;

    @Bean
    public OpenAPI customOpenApi() {
        var securitySchemeName = "credential_schema";
        var servers = List.of(new Server().url(serverUrl));
        return new OpenAPI()
                .servers(servers)
                .components(new Components().addSecuritySchemes(
                        securitySchemeName, new SecurityScheme()
                                .type(SecurityScheme.Type.OAUTH2)
                                .description("Oauth2 Flow")
                                .flows(new OAuthFlows()
                                        .clientCredentials(
                                                new OAuthFlow()
                                                        .tokenUrl(Optional.ofNullable(tokenUrl)
                                                                .orElseThrow(() -> new RuntimeException("Missing token-url")))
                                        )
                                )
                )) .security(List.of(new SecurityRequirement().addList(securitySchemeName)));
    }


}
