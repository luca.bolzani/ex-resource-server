package com.example.resourceserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExResourceServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExResourceServerApplication.class, args);
    }

}
